# [Jekyll Stripe Template](https://gitlab.com/afterlife/jekyll-stripe-template)

### [Live demo](https://afterlife.gitlab.io/jekyll-stripe-template)
This template is meant to help anyone who wishes to create a simple online store using Jekyll with Stripe as the payment gateway/solution.

## Requirements
* NodeJS v6.x
* npm v3.x
* Ruby v2.x

## Usage

Clone the repository

```sh
$ git clone https://gitlab.com/afterlife/jekyll-stripe-template.git
```

Install Ghast submodule

```sh
$ git submodule update --init
```

Run the server

```sh
$ jekyll serve
```
